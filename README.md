# TEST Eukles

* * *

## Table des matières

[1. Enoncé complet](#1-enoncé-complet)

[2. Question 1](#2-question-1)

[3. Question 2](#3-question-2)

[4. Question 3](#4-question-3)

[5. Question 4](#5-question-4)

[6. Projet](#6-projet)

-   [Choix techniques](#choix-techniques)

-   [Modèle de données](#modèle-de-données)

-   [Diagramme de classes](#diagramme-de-classes)

-   [Environement docker](#environnement-docker)

[7. Installation](#7-installation)

* * *

## 1. Enoncé complet

Cliquez [ici](./documentation/ENONCE.md), pour accéder à l'énoncé complet du test.

* * *

## 2. Question 1

> Sélectionner en, utilisant les tables \`client\` , \`materiel\` et \`lien\` (effectuer une jointure) les enregistrements dont les clients ont plus de 30 matériels, et dont le matériel vendu est supérieur à 30.000 euros.

Il est demandé de fournir une requête SQL qui peut être sujette à interprétation.
En effet la partie `dont le matériel vendu est supérieur à 30.000 euros`, peut se comprendre de deux manières :

-   le prix de **chaque** matériel vendu au client est supérieur à 30000 euros.
-   le **total** du matériel vendu au client est supérieur à 30000 euros.

Il en résulte les deux requêtes SQL suivantes :

```sql
-- selectionner les entrées de la table `lien` pour
-- les clients ayant plus de 30 matériels
-- et dont chaque matériel vendu dépasse 30000
SELECT l.client_id, l.materiel_id
FROM `lien` AS l
JOIN `materiel` AS m
	ON m.id = l.materiel_id
WHERE m.price > 30000
AND l.client_id IN (
    SELECT client_id
    FROM `lien`
    GROUP BY client_id
    HAVING COUNT(*) > 30
);
```

```sql
-- selectionner les entrées de la table `lien` pour
-- les clients ayant plus de 30 matériels
-- et dont le total du matériel vendu dépasse 30000
SELECT client_id, materiel_id
FROM `lien`
WHERE client_id IN (
    SELECT l.client_id
	FROM `lien` AS l
	JOIN `materiel` AS m
		ON m.id = l.materiel_id
	GROUP BY client_id 
		HAVING
    	SUM(m.price) > 30000 AND COUNT(*) > 30
);
```

* * *

## 3. Question 2

> Créer la page qui contiendra les formulaires pour saisir le matériel de chaque client. Il est préférable d'utiliser des listes déroulantes pour choisir les clients et les matériels.

Il est précisé dans l'énoncé de la question :

-   "Créer `la` page..."
-   "...`les` formulaires"

Cependant, un seul formulaire basé sur `ProductToCustomerType` sera créé pour sélectionner le client et le matériel à ajouter.

**adresse: `127.0.0.1:8080/client/ajout de matériel`**

* * *

## 4. Question 3

### tableau de total ventes par client

> ...calculer et afficher les totaux vendus pour chaque client...

En SQL nous obtenons la requête suivante :

```SQL
SELECT c.name, SUM(m.price * l.quantity) AS "total"
FROM client AS c
JOIN lien AS l
    ON l.client_id = c.id
JOIN materiel AS m
    ON m.id = l.materiel_id
GROUP BY c.id
ORDER BY c.id;
```

Qui se transpose ainsi en DQL :

```php
$this->createQueryBuilder('c')
    ->select('c.name')
    ->addSelect('SUM(m.price * l.quantity) as total')
    ->join('c.lien', 'l')
    ->join('l.materiel', 'm')
    ->groupBy('c.id')
    ->orderBy('c.id')
    ->getQuery()
    ->getScalarResult();
```

### en-tête du meilleur client

> ..afficher  le client le plus rentable..

Il suffit de modifier légèrement la requête SQL précédente :

```SQL
SELECT c.id, SUM(m.price * l.quantity) AS "total"
FROM client AS c
JOIN lien AS l
    ON l.client_id = c.id
JOIN materiel AS m
    ON m.id = l.materiel_id
GROUP BY c.id
ORDER BY total DESC
LIMIT 1;
```

Qui se transpose ainsi en DQL :

```php
$this->createQueryBuilder('c')
    ->select('c.name')
    ->addSelect('SUM(m.price * l.quantity) as total')
    ->join('c.lien', 'l')
    ->join('l.materiel', 'm')
    ->groupBy('c.id')
    ->orderBy('total', 'DESC')
    ->setMaxResults(1)
    ->getQuery()
    ->getResult();
```

**adresse: `127.0.0.1:8080/client/stats`**

* * *

## 5. Question 4

> Ajouter quelques vérifications et retours utilisateur en javascript, sur les formulaires, permettant de contrôler la saisie (prix négatifs, champs vides etc.).

Pour satisfaire la demande de contrôle de la saisie du prix, une page de création de matériel est créée.

**adresse: `127.0.0.1:8080/materiel/nouveau`**

* * *

## 6. Projet

### Choix techniques

-   PHP 8.1.
-   Symfony 6.1.
-   Postgresql 13-alpine.
-   Pipeline d'intégration continue gitlab-ci (suivre le lien: [intégration continue](./documentation/CONTINUOUS_INTEGRATION.md)).
-   Hook de pre-commit [Grumphp](https://github.com/phpro/grumphp).

### Modèle de données

![data-model](./documentation/assets/data-model/data-model.png)

### Diagramme de classes

![class-diagram](./documentation/assets/class-diagram/class-diagram.png)

### Environnement Docker

Cliquez [ici](./documentation/DOCKER.md), pour plus de détails sur l'environement docker.

* * *

## 7. Installation

Cloner le projet :

```bash
git clone git@gitlab.com:phil-all/test-eukles.git
```

Lancer l'environement docker :

```bash
# root project
docker-compose build && \
docker-compose up -d

# or use make command
make start
```

Lancer le bash dockerizé de l'application symfony :

```bash
# root project
docker exec -it eukles_php bash

# or use make command
make bash
```

Les étapes suivantes sont nécessaires:

-   Créer la base de données.
-   Effectuer la migration dans la base créée.
-   Hydrater la base avec un jeu de données démo (cliquez [ici](./documentation/FIXTURES.md) pour plus de détails).

Pour se faire :

```bash
# in dockerized app bash
make db
```

Le projet est accessible sur navigateur à l'adresse :
**127.0.0.1:8080**
