<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ClientRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: ClientRepository::class)]
class Client
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 120)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Lien::class, orphanRemoval: true)]
    private Collection $lien;

    public function __construct()
    {
        $this->lien = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection<int, Lien>
     */
    public function getLien(): Collection
    {
        return $this->lien;
    }

    public function addLien(Lien $lien): self
    {
        if (!$this->lien->contains($lien)) {
            $this->lien->add($lien);
            $lien->setClient($this);
        }

        return $this;
    }

    public function removeLien(Lien $lien): self
    {
        if ($this->lien->removeElement($lien)) {
            // set the owning side to null (unless already changed)
            if ($lien->getClient() === $this) {
                $lien->setClient(null);
            }
        }

        return $this;
    }
}
