<?php

namespace App\Controller;

use App\Entity\Materiel;
use App\Form\MaterielFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;

class MaterielController extends AbstractController
{
    #[Route('/materiel/nouveau', name: 'app_materiel_create')]
    public function index(Request $request, PersistenceManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(MaterielFormType::class)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var Materiel $materiel */
            $materiel = new Materiel();

            $materiel->setName($form->get('name')->getData())
                ->setPrice($form->get('price')->getData());

            $em = $doctrine->getManager();
            $em->persist($materiel);
            $em->flush();

            return new RedirectResponse($request->headers->get('referer'));
        }

        return $this->render('materiel/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
