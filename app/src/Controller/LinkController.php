<?php

namespace App\Controller;

use App\Entity\Lien;
use App\Entity\Client;
use App\Entity\Materiel;
use App\Form\LienFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;

class LinkController extends AbstractController
{
    #[Route('/client/ajout-materiel', name: 'app_client_add_materiel')]
    public function addCustomerProduct(Request $request, PersistenceManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(LienFormType::class)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var Lien $lien */
            $lien = new Lien();

            $lien->setClient($form->get('Client')->getData())
                ->setMateriel($form->get('Materiel')->getData())
                ->setQuantity($form->get('quantity')->getData())
                ->setCreatedAt(new \DateTimeImmutable('now'));

            $em = $doctrine->getManager();
            $em->persist($lien);
            $em->flush();

            return new RedirectResponse($request->headers->get('referer'));
        }

        return $this->render('client/add-materiel.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
