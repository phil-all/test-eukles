<?php

namespace App\Controller;

use App\Entity\Client;
use App\Repository\ClientRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;

class ClientController extends AbstractController
{
    #[Route('/client/stats', name: 'app_client_stats')]
    public function index(PersistenceManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();

        /** @var ClientRepository $repository */
        $repository = $em->getRepository(Client::class);

        $list = $repository->findAllClientsTotalSale();
        $best = $repository->findBestClient();

        return $this->render('client/stats.html.twig', [
            'bestCustomer' => $best,
            'allCustomers' => $list
        ]);
    }
}
