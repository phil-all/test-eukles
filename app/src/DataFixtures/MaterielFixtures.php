<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Materiel;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class MaterielFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $name = 'materiel_' . strval($i + 1);

            $product = new Materiel();
            $product
                ->setName($name)
                ->setPrice($faker->randomFloat(2, 50, 1500));

            $this->addReference($name, $product);
            $manager->persist($product);
        }

        $manager->flush();
    }
}
