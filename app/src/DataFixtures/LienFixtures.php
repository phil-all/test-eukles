<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Lien;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class LienFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 25; $i++) {
            $customer = $this->getReference('client_' . strval($i + 1));

            for ($j = 0; $j < rand(1, 15); $j++) {
                $link = new Lien();

                $link
                    ->setClient($customer)
                    ->setMateriel($this->getReference('materiel_' . $faker->numberBetween(1, 100)))
                    ->setQuantity($faker->numberBetween(1, 15))
                    ->setCreatedAt(new \DateTimeImmutable('now'));

                $manager->persist($link);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ClientFixtures::class,
        ];
    }
}
