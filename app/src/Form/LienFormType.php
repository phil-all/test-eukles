<?php

namespace App\Form;

use App\Entity\Lien;
use App\Entity\Client;
use App\Entity\Materiel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class LienFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Client', EntityType::class, [
                'class' => Client::class,
                'placeholder' => 'Sélectionner un client dans la liste',
            ])
            ->add('Materiel', EntityType::class, [
                'class' => Materiel::class,
                'placeholder' => 'Sélectionner un matériel dans la liste',
                'label' => 'Matériel',
                'choice_label' => function ($materiel) {
                    return $materiel->getName() . ' : ' . $materiel->getPrice() . ' €';
                }
            ])
            ->add('quantity', NumberType::class, [
                'label' => 'Quantité',
                'constraints' => [new Positive()],
                'attr' => [
                    'placeholder' => 'Renseignez une quantité',
                ]
            ]);
    }

    // public function configureOptions(OptionsResolver $resolver): void
    // {
    //     $resolver->setDefaults([
    //         'data_class' => Lien::class,
    //     ]);
    // }
}
