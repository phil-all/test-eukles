# Environement Docker

[Retour](../README.md).

## Containers

### Database

Le serveur de base de données est construit à partir d'une image `postgres-14-alpine`.

### Nginx

Reverse proxy server nginx: `latest`.

### Php

Le serveur de l'appication est construit à partir d'une image `php:8.1-fpm`.
