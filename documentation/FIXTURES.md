# Jeu de données démos

[Retour](../README.md).

Les datas fixtures installent les données suivantes:

-   100 matériels (prix aléatoire entre 50 et 1500 euros).

-   25 clients, et affecte de manière aléatoire entre 1 et 15 matériels par client.

Réinitialiser la base et les données démos :

```bash
# in dockerized app bash
make db
```
