# Intégration continue

[Retour](../README.md).

Un pipeline gitlab-ci est utilisé pour l'intégration continue.

```bash
project/
├─ app/
│  └─ .gitlab-ci.yml    # pipeline secondaire.
└─ .gitlab-ci.yml       # pipeline principal, fait appel au pipeline secondaire.
```

Celui-ci utilise une image docker ( [jakzal/phpqa:php8.1](https://github.com/jakzal/phpqa) ) et comporte plusieurs étages:

-   Sécurité:
    -   security checker

-   Standards de code:
    -   phpcs
    -   phpstan
