# Enoncé complet

[Retour](../README.md).

1.  Sélectionner en, utilisant les tables \`client\` , \`materiel\` et \`lien\` (effectuer une jointure) les enregistrements dont les clients ont plus de 30 matériels, et dont le matériel vendu est supérieur à 30.000 euros.

2.  Créer la page qui contiendra les formulaires pour saisir le matériel de chaque client. Il est préférable d'utiliser des listes déroulantes pour choisir les clients et les matériels.

3.  Créer la page qui calcule et affiche les totaux vendus pour chaque client, avec le client le plus rentable

4.  Ajouter quelques vérifications et retours utilisateur en javascript, sur les formulaires, permettant de contrôler la saisie (prix négatifs, champs vides etc.).

Le projet devra obligatoirement respecter ces 3 critères :

-   utilisation d’un framework PHP (libre choix)
-   utilisation d’un ORM (libre choix)
-   se lancer en une ligne de commande via docker
